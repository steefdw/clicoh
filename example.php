<?php
/**
 * clicoh (c) Steef de Winter 2018
 * User: steef
 * Date: 18-2-18
 * Time: 15:57
 */

if(!file_exists(__DIR__ . '/vendor/autoload.php'))
    die("\033[31mPlease run 'composer install' first\033[0m".PHP_EOL);
require_once __DIR__ . '/vendor/autoload.php';

$message = 'hello world!';
$colors = [
    'default',
    'black',
    'dark gray',
    'light gray',
    'white',
    'red',
    'light red',
    'magenta',
    'light magenta',
    'blue',
    'light blue',
    'cyan',
    'light cyan',
    'green',
    'light green',
    'yellow',
    'light yellow',
];

use Clicoh\Alert;
use Clicoh\Output;


echo "
Output::text() without options:
";
echo Output::text($message);
echo "
____________

Output::text() with a color:
";

foreach($colors as $color)
{
    echo Output::text($message, $color).' ';
}
echo "
____________

Output::text() with an option:
";
echo Output::text($message, 'red', 'bold')
    .' '.
    Output::text($message, 'red', 'underline')
    .' '.
    Output::text($message, 'red', 'dim')
;

echo "
____________

Alerts:
";
echo Alert::success('success');
echo Alert::warn('warn');
echo Alert::info('info');

echo "____________

All background colors: ";
foreach($colors as $color)
{
    echo Output::background(':)', $color, 'black');
}
echo "
All foreground colors: ";
foreach($colors as $color)
{
    echo Output::text(':)', $color);
}


echo "
____________

Doing stuff manually:
";


echo
    Output::text('H', 'light red').
    Output::text('e', 'red').
    Output::text('l', 'magenta').
    Output::text('l', 'light magenta').
    Output::text('o', 'cyan').
    Output::text('.', 'light cyan').
    Output::text('W', 'blue').
    Output::text('o', 'light blue').
    Output::text('r', 'green').
    Output::text('l', 'light green').
    Output::text('d', 'yellow').
    Output::text('!', 'light yellow').
    PHP_EOL;

echo
    Output::background('H', 'light red').
    Output::background('e', 'red').
    Output::background('l', 'magenta').
    Output::background('l', 'light magenta').
    Output::background('o', 'cyan').
    Output::background('.', 'light cyan').
    Output::background('W', 'blue').
    Output::background('o', 'light blue').
    Output::background('r', 'green').
    Output::background('l', 'light green').
    Output::background('d', 'yellow').
    Output::background('!', 'light yellow').
    PHP_EOL;

echo
    Output::background('H', 'light red', 'black'),
    Output::background('e', 'red', 'black'),
    Output::background('l', 'magenta', 'black'),
    Output::background('l', 'light magenta', 'black'),
    Output::background('o', 'cyan', 'black'),
    Output::background('.', 'light cyan', 'black'),
    Output::background('W', 'blue', 'black'),
    Output::background('o', 'light blue', 'black'),
    Output::background('r', 'green', 'black'),
    Output::background('l', 'light green', 'black'),
    Output::background('d', 'yellow', 'black'),
    Output::background('!', 'light yellow', 'black'),
    PHP_EOL;
