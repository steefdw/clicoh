<?php
/**
 * Clicoh - The CLI colored output helper
 *
 * @see: https://gitlab.com/steefdw/clicoh
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/clicoh/blob/master/LICENCE
 *
 * Date: 19-2-18
 */

namespace Clicoh;

class OutputTest extends \PHPUnit_Framework_TestCase {

    public function testOutputTextWithoutOptions()
    {
        $shouldBe = "\033[0;49;39mtest\033[0m";
        $output = Output::text('test');

        $this->assertEquals(
            $output,
            $shouldBe,
            'Output::text() should return the same text when executed without options'
        );
    }

    public function testOutputTextInBlueText()
    {
        $shouldBe = "\033[0;49;34mtest\033[0m";
        $output = Output::text('test', 'blue');

        $this->assertEquals(
            $output,
            $shouldBe,
            'Output::text(\'test\', \'blue\') should return "test" in blue'
        );
    }

    public function testOutputTextInBoldBlueText()
    {
        $shouldBe = "\033[1;49;34mtest\033[0m";
        $output = Output::text('test', 'blue', 'bold');

        $this->assertEquals(
            $output,
            $shouldBe,
            'Output::text(\'test\', \'blue\', \'bold\') should return bold "test" in the color blue'
        );
    }

    public function testOutputTextInUnderlineBlueText()
    {
        $shouldBe = "\033[4;49;34mtest\033[0m";
        $output = Output::text('test', 'blue', 'underline');

        $this->assertEquals(
            $output,
            $shouldBe,
            'Output::text(\'test\', \'blue\', \'bold\') should return underlined "test" in the color blue'
        );
    }

    public function testOutputGreenBackgroundInUnderlineBlueText()
    {
        $shouldBe = "\033[4;42;34mtest\033[0m";
        $output = Output::background('test', 'green', 'blue', 'underline');

        $this->assertEquals(
            $output,
            $shouldBe,
            'Output::background(\'test\', \'green\', \'blue\', \'underline\') should return underlined "test" with a green background in blue text'
        );
    }

    public function testOutputSetGreenBackgroundInDimBlueText()
    {
        $shouldBe = "\033[2;42;34mtest\033[0m";
        $output = Output::set('test', 'blue', 'green', 'dim');

        $this->assertEquals(
            $output,
            $shouldBe,
            'Output::set(\'test\', \'blue\', \'green\', \'dim\') should return a dimmed "test" with a green background in blue text'
        );
    }

}
