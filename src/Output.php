<?php
/**
 * Clicoh - The CLI colored output helper
 *
 * @see: https://gitlab.com/steefdw/clicoh
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/clicoh/blob/master/LICENCE
 *
 * Date: 18-2-18
 */


namespace Clicoh;

/**
 * Colorize the output in the terminal
 *
 * For more info about colors in the terminal, see https://misc.flogisoft.com/bash/tip_colors_and_formatting
 *
 * @property string $background
 * @property array $backgrounds
 * @property string $color
 * @property array $colors
 * @property string $option
 * @property array $options
 */
class Output {

    private $background = '49';
    private $backgrounds = [
        '49' => 'default',
        '40' => 'black',
        '41' => 'red',
        '42' => 'green',
        '43' => 'yellow',
        '44' => 'blue',
        '45' => 'magenta',
        '46' => 'cyan',
        '47' => 'light gray',
        '100' => 'dark gray',
        '101' => 'light red',
        '102' => 'light green',
        '103' => 'light yellow',
        '104' => 'light blue',
        '105' => 'light magenta',
        '106' => 'light cyan',
        '107' => 'white',
    ];
    private $color = '39';
    private $colors = [
        '39' => 'default',
        '30' => 'black',
        '31' => 'red',
        '32' => 'green',
        '33' => 'yellow',
        '34' => 'blue',
        '35' => 'magenta',
        '36' => 'cyan',
        '37' => 'light gray',
        '90' => 'dark gray',
        '91' => 'light red',
        '92' => 'light green',
        '93' => 'light yellow',
        '94' => 'light blue',
        '95' => 'light magenta',
        '96' => 'light cyan',
        '97' => 'white',
    ];
    private $option = '0';
    private $options = [
        '0' => 'normal',
        '1' => 'bold',
        '2' => 'dim',
        '4' => 'underline',
        '5' => 'blink',     // does not work with most of the terminal emulators (phew!), works in the tty and XTerm.
        '7' => 'reverse',   // invert the foreground and background colors
        '8' => 'hidden',    // hide the text: useful for passwords
    ];

    /**
     * @param $message
     * @param string $color
     * @param string $background
     * @param string $option
     * @return string
     */
    static function set($message, $color = 'default', $background = 'default', $option = 'normal')
    {
        $output = new Output;

        return $output
            ->setBackground($background)
            ->setColor($color)
            ->setOption($option)
            ->message($message);
    }

    /**
     * @param string $text
     * @param string $textColor
     * @return string
     */
    static function text($text, $textColor = 'default', $option = 'normal')
    {
        return Output::set($text, $textColor, false, $option);
    }

    /**
     * @param string $text
     * @param string $backgroundColor
     * @param string $textColor
     * @param string $option
     * @return string
     */
    static function background($text, $backgroundColor, $textColor = 'default', $option = 'normal')
    {
        return Output::set($text, $textColor, $backgroundColor, $option);
    }

    /**
     * Foreground colors: default, black, red, green, yellow, blue, magenta, cyan, white,
     * light gray, dark gray, light red, light green, light yellow,
     * light blue, light magenta, light cyan
     *
     * @param string $color
     * @return Output
     */
    public function setColor($color)
    {
        if($id = array_search($color, $this->colors))
            $this->color = $id;

        return $this;
    }

    /**
     * Background colors: default, black, red, green, yellow, blue, magenta, cyan, white,
     * light gray, dark gray, light red, light green, light yellow,
     * light blue, light magenta, light cyan
     *
     * @param string $background
     * @return Output
     */
    public function setBackground($background)
    {
        if($id = array_search($background, $this->backgrounds))
            $this->background = $id;

        return $this;
    }

    /**
     * options: 0: Normal, 1: Bold, 2: Dim, 4: Underscore, 5: Blink, 7: Inverse, and 8: Concealed
     *
     * @param string $option
     * @return Output
     */
    public function setOption($option)
    {
        if($id = array_search($option, $this->options))
            $this->option = $id;

        return $this;
    }

    /**
     * @param string $message
     * @return string
     */
    public function message($message)
    {
        $this->check();

        $isRunningInConsole = (php_sapi_name() === 'cli');
        if($isRunningInConsole)
        {
            return"\033[".$this->getFormat()."m{$message}\033[0m";
        }

        return '<span style="background:'.$this->cssColor($this->backgrounds[$this->background]).
            ';color:'.$this->cssColor($this->colors[$this->color]).'">'.
            nl2br($message).
        '</span>';
    }

    /**
     * @return string
     */
    private function getFormat()
    {
        return implode(';', [
            $this->option,
            $this->background,
            $this->color,
        ]);
    }

    /**
     * Example: "light blue" should be "lightblue" in HTML styles
     *
     * @param $name
     * @return mixed
     */
    private function cssColor($name)
    {
        return str_replace(' ', '', $name);
    }

    /**
     * Light text on light backgrounds look awful, so give those black text
     * when the color is not explicitly set
     */
    private function check()
    {
        $background = $this->backgrounds[$this->background];
        $color = $this->colors[$this->color];

        if($background !== 'default' AND $color === 'default')
        {
            $this->color = in_array($background, [
                'green', 'white', 'yellow', 'cyan',
                'light green', 'light yellow', 'light cyan',
            ])
                ? '30'  // black
                : '97'; // white
        }
    }

}
