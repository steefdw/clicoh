# Clicoh - The CLI Colored Output Helper

Colorize the output in the terminal with ease. 

Want red text in your terminal? 
Just echo `Output::text('my text', 'red')` and you're done.

Want white text with a blue background in your terminal? 
Just echo `Output::background('my text', 'blue', 'white')` and you're done.

With Clicoh it's easy to do stuff like this:

![example](https://gitlab.com/steefdw/clicoh/uploads/46f21ee9a47c9e10ead17657d3d9e467/example.png)

## Install

```composer require steefdw/clicoh```

## Usage
Before you can use Clicoh, you should tell PHP you want to use this package:
```php
use Clicoh\Alert;
use Clicoh\Output;
```
## Let's get started!

```php
echo Output::text('hello world!');
```
produces:
![output-text](https://gitlab.com/steefdw/clicoh/uploads/7412cc1071f7beddd2bcca61a450b88a/output-text.png)


```php
echo Output::text('hello world!', 'red');
```
produces:
![output-text-red](https://gitlab.com/steefdw/clicoh/uploads/70627fae4f5e2f67f3e83657b64ed7ac/output-text-red.png)

It's just an alias for:
```php
echo Output::set('hello world!', 'red');
```
![output-text-red](https://gitlab.com/steefdw/clicoh/uploads/70627fae4f5e2f67f3e83657b64ed7ac/output-text-red.png)

So why not use `Output::set()` directly for colored text? Because it's easier to set the options:
```php
echo Output::text('hello world!', 'red', 'bold');
```
![output-text-red-bold](https://gitlab.com/steefdw/clicoh/uploads/56cbd933d8c1877ac9cef7b4f4d29552/output-text-red-bold.png)

```php
echo Output::text('hello world!', 'red', 'underline');
```
![output-text-red-undeline](https://gitlab.com/steefdw/clicoh/uploads/a6d8399c1e28912186b896935f205354/output-text-red-undeline.png)

```php
echo Output::text('hello world!', 'red', 'dim');
```
![output-text-red-dim](https://gitlab.com/steefdw/clicoh/uploads/3a18c096e4ab19bfd0d5434bfe0f4f4f/output-text-red-dim.png)

### Background colors

Setting the background color is easy as pi:
```php
echo Output::background('hello world!', 'green');
```
![output-bg-green](https://gitlab.com/steefdw/clicoh/uploads/fa1137197623a84057cb9fb0c255785d/output-bg-green.png)

You can also set the text color like this:
```php
echo Output::background('hello world!', 'green', 'black');
```
![output-success](https://gitlab.com/steefdw/clicoh/uploads/7eb49fcb3459c9e964c17caa94188603/output-success.png)

Because that is looking great for "success" messages, it's aliased wih `Alert::success()`:
```php
echo Alert::success('hello world!');
```
![output-success](https://gitlab.com/steefdw/clicoh/uploads/7eb49fcb3459c9e964c17caa94188603/output-success.png)

## Alerts
`Alert::success()` is very great for understanding what will happen if you read your code, so why not other alerts?
At this moment there are three alert types: `success`, `warn`, and `info`.

Warning: Alert::*() will add a new line after the message.

```php
echo Alert::success('hello world!');
```
![output-success](https://gitlab.com/steefdw/clicoh/uploads/7eb49fcb3459c9e964c17caa94188603/output-success.png)


```php
echo Alert::warn('hello world!');
```
![output-warn](https://gitlab.com/steefdw/clicoh/uploads/b4a43c8c9f965fa4a6c576e894a3bfd7/output-warn.png)

```php
echo Alert::info('hello world!');
```
![output-info](https://gitlab.com/steefdw/clicoh/uploads/2c191c90d88f5187c7717c793508858e/output-info.png)

## Advanced options

So Output::text(), Output::background() and Alert::*() are just aliases for Output::set().
When using `Output::set()` you can can go crazy, and also set options like underline, bold:
```php
echo Output::set('hello world!', 'black', 'green', 'underline');
```
![output-bg-green-black-underline](https://gitlab.com/steefdw/clicoh/uploads/64d578063110701c2d2560edd5df54b1/output-bg-green-black-underline.png)

In fact, `Alert::warn()` is just an alias for:
```php
echo Output::set('hello world!', 'white', 'red', 'bold').PHP_EOL;
```
![output-warn](https://gitlab.com/steefdw/clicoh/uploads/b4a43c8c9f965fa4a6c576e894a3bfd7/output-warn.png)

And `Output::text('hello world!', 'red', 'dim')` is actually just an alias for
```php
echo Output::set('hello world!', 'red', 'default', 'dim');
```
![output-text-red-dim](https://gitlab.com/steefdw/clicoh/uploads/3a18c096e4ab19bfd0d5434bfe0f4f4f/output-text-red-dim.png)


## Let's go crazy
It is really easy to colorize your CLI php application with Clicoh
```php
echo
    Output::text('H', 'light red').
    Output::text('e', 'red').
    Output::text('l', 'magenta').
    Output::text('l', 'light magenta').
    Output::text('o', 'cyan').
    Output::text('.', 'light cyan').
    Output::text('W', 'blue').
    Output::text('o', 'light blue').
    Output::text('r', 'green').
    Output::text('l', 'light green').
    Output::text('d', 'yellow').
    Output::text('!', 'light yellow')
;
```
![output-rainbow](https://gitlab.com/steefdw/clicoh/uploads/5477040c50ec8434e288576f295c538e/output-rainbow.png)

## Example
In the root of this project is an `example.php` that shows you how you can easily use the functionality of Clicoh. 

If you run it in your terminal with `php example.php` you should have this output in your terminal:
![example](https://gitlab.com/steefdw/clicoh/uploads/46f21ee9a47c9e10ead17657d3d9e467/example.png)