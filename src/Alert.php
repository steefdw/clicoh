<?php
/**
 * Clicoh - The CLI colored output helper
 *
 * @see: https://gitlab.com/steefdw/clicoh
 * @author Steef de Winter
 * @copyright Copyright (c) 2018 Steef de Winter
 * @licence: https://gitlab.com/steefdw/clicoh/blob/master/LICENCE
 *
 * Date: 18-2-18
 */

namespace Clicoh;

class Alert {

    /**
     * @param string $message
     * @return string
     */
    static function warn($message)
    {
        return Output::set($message, 'white', 'red', 'bold') . PHP_EOL;
    }

    /**
     * @param string $message
     * @return string
     */
    static function success($message)
    {
        return Output::set($message, 'black', 'green') . PHP_EOL;
    }

    /**
     * @param string $message
     * @return string
     */
    static function info($message)
    {
        return Output::set($message, 'black', 'cyan') . PHP_EOL;
    }

}